$(document).ready(function(){
  $("#blank form").submit(function(event){
    event.preventDefault();

    $("#hide").hide();

    var puzzleInputs = $("#puzzleAnswer").val().split("");
    var vowels = ["a", "e", "i", "o", "u"];
    var puzzleOutputs = [];

    for (var letter = 0; letter < puzzleInputs.length; letter += 1) {
      for (var index = 0; index < vowels.length; index += 1 ) {
        if (puzzleInputs[letter] === vowels[index]) {
          puzzleOutputs.push("-");
          letter += 1;
          index = -1; //lolwut? this resets to the first index in vowel array. it works we swear
        }
      }
      puzzleOutputs.push(puzzleInputs[letter]);
    }
    puzzleOutputs.join("");
    $(".output").append(puzzleOutputs);
  });
});
