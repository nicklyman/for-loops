$(document).ready(function() {
  $("#blanks form").submit(function(event) {
    event.preventDefault();

    var countToInput = parseInt($("#countTo").val());
    var countByInput = parseInt($("#countBy").val());

    if (countToInput <= 0 || countByInput <= 0) {
      alert("Please enter a number greater than 0.")
    }
    if (isNaN(countToInput) || isNaN(countByInput)) {
      alert("Please enter a number.")
    }
    if (countByInput > countToInput) {
      alert("Make sure the number you count to is greater than the number you count by.")
    }
    for (var index = 0; index <= countToInput; index += countByInput) {
      $(".output1").append("<p>" + index + "</p>");
    }
  });


  // for (var index = 0; index <= 30; index += 5) {
  // $(".output1").append("<p>" + index + "</p>");
  // };
  // for (var index = 0; index <= 50; index += 7) {
  // $(".output2").append("<p>" + index + "</p>");
  // };
});
